# README #

Aplikacje demo SoftwareSkill dla modułu obsługi baz danych - częśc dotycząca prostych popularnych frameworków takich jak MyBatis oraz JdbcTemplate bedących frameworkami typu Persistence Framework.

### Po co to repozytorium? ###

Repozytorium prezentuje sposób konfiguracji i wykorzystania API danego frameworka  
 
* Wersja 1.0

### Opis ###
* Funkcjonalność - aplikacje w ramach modułów realizują tą samą kogikę, wyszukują w bazie danych karty o podanym identyfikatorze i wyświetlają jej dane. W przypadku gdy karta nie zostanie znaleziona wyświetlana jest informacja iż danych dla danego identyfikatora nie znaleziono.
* Baza danych - baza danych zawierająca informacje o kartach (identyfikator jako klucz, UUID karty, dane właściciela karty).
* Konfiguracja - nie ma konieczności konfiguracji. Wykorzystywana jest baza H2 w trybie in memory.  
* Zależności -  H2 (baza danych in memory), Mockito, JUnit 5, Spring Boot, Logback (framework do logów aplikacyjnych).
* Jak uruchomić testy - polecenie mvn test lub uruchomić z poziomu IDE (np. Run All tests w InbtelliJ dla modułu).
* Jak uruchomić aplikację - z linii poleceń klasy, mając w nazwie Appliaction z parametrem równym 1 (identyfikator karty) 

### Z kim się kontaktować? ###

* Właściciel repozytorium kodu - SoftwareSkill
* Autorzy rozwiązania - Krzysztof Kądziołka 