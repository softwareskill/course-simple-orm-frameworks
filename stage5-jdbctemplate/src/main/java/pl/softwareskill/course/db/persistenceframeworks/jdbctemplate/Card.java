package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

import lombok.Value;

@Value
class Card {

    String cardId;
    String cardUuid;
    String cardOwner;
    boolean enabled;
    CardCountry cardCountry;
}
