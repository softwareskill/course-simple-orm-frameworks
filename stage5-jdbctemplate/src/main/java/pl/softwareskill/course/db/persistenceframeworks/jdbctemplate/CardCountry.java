package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

public enum CardCountry {
    PL,
    EN,
    DE
}
