package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
class CardRepositoryImpl extends AbstractRepository<Card, String> implements CardRepository {

    CardRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate, new CardMapper());
    }

    @Override
    protected String findByIdQuery(String key) {
        return "select * from cards where card_id='" + key + "'";
    }

}