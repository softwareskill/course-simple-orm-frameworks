package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

import java.util.Optional;

interface CardRepository extends Repository<Card, String> {

    Optional<Card> findById(String cardId);
}
