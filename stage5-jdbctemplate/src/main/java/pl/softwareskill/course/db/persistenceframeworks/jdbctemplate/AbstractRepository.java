package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.util.Optional;

import static java.util.Optional.ofNullable;
import static lombok.AccessLevel.PRIVATE;

@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor
public abstract class AbstractRepository<T, K> implements Repository<T, K> {

    JdbcTemplate jdbcTemplate;
    ResultSetExtractor<T> mapper;

    protected abstract String findByIdQuery(K key);

    @Override
    public Optional<T> findById(K key) {
        return ofNullable(jdbcTemplate.query(findByIdQuery(key), mapper));
    }
}