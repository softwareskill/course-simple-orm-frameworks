package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

class DataMappingException extends RuntimeException {

    DataMappingException(Throwable cause) {
        super(cause);
    }
}
