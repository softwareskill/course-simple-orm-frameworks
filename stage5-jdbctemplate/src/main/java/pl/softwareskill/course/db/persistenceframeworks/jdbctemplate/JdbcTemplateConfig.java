package pl.softwareskill.course.db.persistenceframeworks.jdbctemplate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@SuppressWarnings("unused")
class JdbcTemplateConfig {

    @Bean
    CardInformationLogger cardInformationLogger() {
        return new CardInformationLogger();
    }

    @Bean
    CardInformationReader cardInformationReader(CardRepository cardRepository) {
        return cardInformationReader(cardInformationLogger(), cardRepository);
    }

    CardInformationReader cardInformationReader(CardInformationLogger logger, CardRepository cardRepository) {
        return new CardInformationReader(logger, cardRepository);
    }
}
