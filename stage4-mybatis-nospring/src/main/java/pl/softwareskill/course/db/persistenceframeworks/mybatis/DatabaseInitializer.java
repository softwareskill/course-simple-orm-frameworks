package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import java.io.IOException;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class DatabaseInitializer {

    public static SqlSession initialize() {
        try {
            var reader = Resources.getResourceAsReader("mybatis-config.xml");
            var sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
            return sqlSessionFactory.openSession();
        } catch (IOException e) {
            throw new DatabaseInitializationException(e);
        }
    }
}
