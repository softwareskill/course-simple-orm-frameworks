package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import org.apache.ibatis.session.SqlSession;

public class MybatisApplication {

    public static void main(String[] args) {
        CardInformationLogger logger = new CardInformationLogger();
        SqlSession session = DatabaseInitializer.initialize();
        CardRepository cardRepository = session.getMapper(CardRepository.class);
        CardInformationReader cardInformationReader = new CardInformationReader(logger, cardRepository);
        cardInformationReader.printCardData(args[0]);
    }

}
