package pl.softwareskill.course.db.persistenceframeworks.mybatis;

class Card {
    private final String cardId;
    private final String cardUuid;
    private final String cardOwner;
    private final boolean enabled;
    private final CardCountry cardCountry;

    Card(String cardId, String cardUuid, String cardOwner, boolean enabled, CardCountry cardCountry) {
        this.cardId = cardId;
        this.cardUuid = cardUuid;
        this.cardOwner = cardOwner;
        this.enabled = enabled;
        this.cardCountry = cardCountry;
    }

    String getCardId() {
        return cardId;
    }

    String getCardUuid() {
        return cardUuid;
    }

    String getCardOwner() {
        return cardOwner;
    }

    boolean isEnabled() {
        return enabled;
    }

    CardCountry getCardCountry() {
        return cardCountry;
    }
}
