package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import java.util.Optional;

interface CardRepository {

    Optional<Card> findById(String cardId);
}
