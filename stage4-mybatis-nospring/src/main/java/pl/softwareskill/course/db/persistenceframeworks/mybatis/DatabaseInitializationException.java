package pl.softwareskill.course.db.persistenceframeworks.mybatis;

class DatabaseInitializationException extends RuntimeException {
    public DatabaseInitializationException(Throwable cause) {
        super(cause);
    }
}
