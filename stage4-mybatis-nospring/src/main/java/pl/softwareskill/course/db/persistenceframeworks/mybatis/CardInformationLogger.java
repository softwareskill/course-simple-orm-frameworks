package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

class CardInformationLogger {

    private static final Logger LOG = getLogger(CardInformationLogger.class);

    void logCardData(Card card) {
        LOG.info("Card data for id={} is uuid={}, ownerName={}, active={}, country={}", card.getCardId(), card.getCardUuid(),
                card.getCardOwner(), card.isEnabled(), card.getCardCountry());
    }

    void logCardDataNotFound(String cardId) {
        LOG.error("Card data for id={} doesn't exist", cardId);
    }
}
