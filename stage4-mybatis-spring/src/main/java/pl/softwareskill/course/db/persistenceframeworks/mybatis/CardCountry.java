package pl.softwareskill.course.db.persistenceframeworks.mybatis;

public enum CardCountry {
    PL,
    EN,
    DE
}
