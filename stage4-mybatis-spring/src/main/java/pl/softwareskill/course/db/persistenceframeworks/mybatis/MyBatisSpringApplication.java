package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class MyBatisSpringApplication implements CommandLineRunner {

    CardInformationReader cardInformationReader;

    public static void main(String[] args) {
        SpringApplication.run(MyBatisSpringApplication.class, args);
    }

    @Override
    public void run(String... args) {
        if (args.length == 1) {
            cardInformationReader.printCardData(args[0]);
        } else {
            log.error("Wymagany jeden parametr ");
        }
    }
}
