package pl.softwareskill.course.db.persistenceframeworks.mybatis;

import javax.sql.DataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;

@Configuration
@MapperScan(annotationClass = Repository.class)
class MyBatisSpringApplicationConfig {

    @Bean
    SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        return factoryBean.getObject();
    }

    @Bean
    CardInformationLogger cardInformationLogger() {
        return new CardInformationLogger();
    }

    @Bean
    CardInformationReader cardInformationReader(CardInformationLogger cardInformationLogger, CardRepository cardRepository) {
        return new CardInformationReader(cardInformationLogger, cardRepository);
    }
}
